# ARMILU (https://ieeexplore.ieee.org/abstract/document/10095062)

ARMILU is an abstract graph representation for multi-intents and multi-domains corpora used in Natural and Spoken Language Understanding (N/SLU) tasks. Penman notation is used to encode the any origin datasets' annotations (simple and complicated ones). Any type of N/SLU corpora can be projected to this format. 

# Citing 

@inproceedings{abrougui2023abstract,

  title={Abstract Representation for Multi-Intent Spoken Language Understanding},

  author={Abrougui, Rim and Damnati, Géraldine and Heinecke, Johannes and Béchet, Frédéric},

  booktitle={ICASSP 2023-2023 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)},

  pages={1--5},

  year={2023},
  
  organization={IEEE} 
}


